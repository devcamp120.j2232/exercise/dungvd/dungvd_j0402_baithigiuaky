package com.devcamp.midqualification.deparmentemployee.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.midqualification.deparmentemployee.model.Department;
import com.devcamp.midqualification.deparmentemployee.model.Employee;
import com.devcamp.midqualification.deparmentemployee.repository.DepartmentRepository;
import com.devcamp.midqualification.deparmentemployee.repository.EmployeeRepository;

@RestController
@CrossOrigin
public class EmployeeController {
    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    DepartmentRepository departmentRepository;
    // API trả về toàn bộ nhân viên
    @GetMapping("/employees")
    public ResponseEntity<List<Employee>> getAllEmployees() {
        try {
            // Tạo list chứa employee
            List<Employee> allEmployees = new ArrayList<Employee>();
            // Dùng hàm findAll để lấy ra toàn bộ employee sau đó dùng hàm forEach add vào
            // list nhân viên vừa tạo
            employeeRepository.findAll().forEach(allEmployees::add);
            // Trả về list nhân viên và trạng thái ok
            return new ResponseEntity<>(allEmployees, HttpStatus.OK);
        } catch (Exception e) {
            // Nếu có lỗi trả về null và trạng thái lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    // API lấy ra danh sách nhân viên thông qua department ID
    @GetMapping("/department/{id}/employees")
    public ResponseEntity<Set<Employee>> getListEmployeeByDepartmentId(@PathVariable("id") long id) {
        try {
            // gọi hàm có sẵn ở để lấy thông tin department và trả về kiểu dữ liệu Department qua hàm .get()
            Department department = departmentRepository.findById(id).get();
            // Trả về list nhân viên của phòng ban đó thông qua hàm .getEmployees();
            return new ResponseEntity<>(department.getEmployees(), HttpStatus.OK);
        } catch (Exception e) {
            // Nếu có lỗi trả về null và trạng thái lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy ra thông tin nhân viên thông qua ID
    @GetMapping("/employee/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") long id) {
        try {
            // dùng hàm findById có sẵn để truy xuất thông tin của Employee thông qua Id
            Optional<Employee> EmployeeData = employeeRepository.findById(id);
            // Kiểm tra xem dữ liệu Employee có tồn tại hay không thông qua hàm
            // isPresent()
            if (EmployeeData.isPresent()) {
                // Nếu có, dùng hàm .get để trả về kiểu dữ liệu Employee
                Employee employee = EmployeeData.get();
                // Trả về Employee và trạng thái OK
                return new ResponseEntity<>(employee, HttpStatus.OK);
            } else {
                // Nếu không tìm thấy trả về trạng thái notfound
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // Nếu có lỗi trả về null và trạng thái lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    

    // API thêm mới 1 nhân viên
    @PostMapping("/department/{departmentId}/employees")
    public ResponseEntity<Employee> createEmployee(@Valid @RequestBody Employee pEmployee, @PathVariable("departmentId") long departmentId) {
        try {
            // Dùng hàm findById để tìm department thông qua departmentId dùng .get() để trả về kiểu dữ liệu Department
            Department department = departmentRepository.findById(departmentId).get();
            // Set department cho pEmployee
            pEmployee.setDepartment(department);
            // Dùng hàm save để tạo mới Employee
            Employee EmployeeCreated = employeeRepository.save(pEmployee);
            // Trả về Employee vừa tạo và trạng thái OK
            return new ResponseEntity<>(EmployeeCreated, HttpStatus.OK);
        } catch (Exception e) {
            // Nếu có lỗi trả về null và trạng thái lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API để update thông tin 1 nhân viên thông qua ID
    @PutMapping("/department/{departmentId}/employee/{id}")
    public ResponseEntity<Employee> updateEmployee(@RequestBody Employee pEmployee, @PathVariable("id") long id, @PathVariable("departmentId") long departmentId) {
        try {
            // Dùng hàm findById để tìm department thông qua departmentId dùng .get() để trả về kiểu dữ liệu Department
            Department department = departmentRepository.findById(departmentId).get();
            // Dùng hàm findByID để lấy thông Employee có id như trên
            Optional<Employee> employeeData = employeeRepository.findById(id);
            // Dùng hàm isPresent để kiểm tra xem có tồn tại Employee
            if (employeeData.isPresent()) {
                // Nếu có dùng .get() để trả về kiểu dữ liệu Employee
                Employee employee = employeeData.get();
                // Tiến hành set các giá trị cho Employee update
                employee.setEmployeeCode(pEmployee.getEmployeeCode());
                employee.setEmployeeName(pEmployee.getEmployeeName());
                employee.setGender(pEmployee.getGender());
                employee.setDepartment(department);
                employee.setDateOfBirth(pEmployee.getDateOfBirth());
                employee.setPosition(pEmployee.getPosition());
                employee.setAddress(pEmployee.getAddress());
                employee.setPhoneNumber(pEmployee.getPhoneNumber());
                // Dùng hàm save() để update thông tin Employee
                Employee employeeUpdated = employeeRepository.save(employee);
                // Trả về Employee và trạng thái OK
                return new ResponseEntity<>(employeeUpdated, HttpStatus.OK);
            } else {
                // Nếu không tìm thấy trả về trạng thái not Found
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // Nếu có lỗi trả về null và trạng thái lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API để xóa 1 employee qua Id
    @DeleteMapping("/employee/{id}")
    public ResponseEntity<Employee> deleteEmployee(@PathVariable("id") long id) {
        try {
            // dùng hàm deleteById để mặc định để xóa employee sau đó trả về trạng thái no content
            employeeRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // Nếu có lỗi trả về null và trạng thái lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API để xóa tất cả nhân viên
    @DeleteMapping("/employees")
    public ResponseEntity<Employee> deleteAllEmployees() {
        try {
            // dùng hàm deleteAll để xóa toàn bộ nhân viên sau đó trả về trạng thái no content
            employeeRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // Nếu có lỗi trả về null và trạng thái lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
