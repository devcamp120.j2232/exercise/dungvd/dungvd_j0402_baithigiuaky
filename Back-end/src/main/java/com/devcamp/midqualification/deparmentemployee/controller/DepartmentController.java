package com.devcamp.midqualification.deparmentemployee.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.midqualification.deparmentemployee.model.Department;
import com.devcamp.midqualification.deparmentemployee.repository.DepartmentRepository;

@RestController
@CrossOrigin
public class DepartmentController {
    @Autowired
    DepartmentRepository departmentRepository;

    // API trả về toàn bộ phòng ban
    @GetMapping("/departments")
    public ResponseEntity<List<Department>> getAllDepartments() {
        try {
            // Tạo list chứa Deparment
            List<Department> allDepartments = new ArrayList<Department>();
            // Dùng hàm findAll để lấy ra toàn bộ department sau đó dùng hàm forEach add vào
            // list phòng ban vừa tạo
            departmentRepository.findAll().forEach(allDepartments::add);
            // Trả về list phòng ban và trạng thái ok
            return new ResponseEntity<>(allDepartments, HttpStatus.OK);
        } catch (Exception e) {
            // Nếu có lỗi trả về null và trạng thái lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy ra thông tin phòng ban thông qua ID
    @GetMapping("/department/{id}")
    public ResponseEntity<Department> getDepartmentById(@PathVariable("id") long id) {
        try {
            // dùng hàm findById có sẵn để truy xuất thông tin của Department thông qua Id
            Optional<Department> departmentData = departmentRepository.findById(id);
            // Kiểm tra xem dữ liệu department có tồn tại hay không thông qua hàm
            // isPresent()
            if (departmentData.isPresent()) {
                // Nếu có, dùng hàm .get để trả về kiểu dữ liệu Department
                Department department = departmentData.get();
                // Trả về department và trạng thái OK
                return new ResponseEntity<>(department, HttpStatus.OK);
            } else {
                // Nếu không tìm thấy trả về trạng thái notfound
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // Nếu có lỗi trả về null và trạng thái lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API tạo mới 1 phòng ban
    @PostMapping("/departments")
    public ResponseEntity<Department> createDepartment(@Valid @RequestBody Department pDepartment) {
        try {
            // Dùng hàm save để tạo mới department
            Department departmentCreated = departmentRepository.save(pDepartment);
            // Trả về deparment vừa tạo và trạng thái OK
            return new ResponseEntity<>(departmentCreated, HttpStatus.OK);
        } catch (Exception e) {
            // Nếu có lỗi trả về null và trạng thái lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API để update 1 phòng ban thông qua ID
    @PutMapping("/department/{id}")
    public ResponseEntity<Department> updateDepartment(@RequestBody Department pDepartment, @PathVariable("id") long id) {
        try {
            // Dùng hàm findByID để lấy thông Department có id như trên
            Optional<Department> departmentData = departmentRepository.findById(id);
            // Dùng hàm isPresent để kiểm tra xem có tồn tại Department
            if (departmentData.isPresent()) {
                // Nếu có dùng .get() để trả về kiểu dữ liệu Department
                Department department = departmentData.get();
                // Tiến hành set các giá trị cho department update
                department.setDepartmentCode(pDepartment.getDepartmentCode());
                department.setDepartmentName(pDepartment.getDepartmentName());
                department.setMajor(pDepartment.getMajor());
                department.setIntroduce(pDepartment.getIntroduce());
                // Dùng hàm save() để update thông tin department
                Department departmentUpdated = departmentRepository.save(department);
                // Trả về department và trạng thái OK
                return new ResponseEntity<>(departmentUpdated, HttpStatus.OK);
            } else {
                // Nếu không tìm thấy trả về trạng thái not Found
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // Nếu có lỗi trả về null và trạng thái lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API để xóa 1 phòng ban thông qua Id
    @DeleteMapping("/department/{id}")
    public ResponseEntity<Department> deleteDepartment(@PathVariable("id") long id) {
        try {
            // dùng hàm deleteById để mặc định để xóa department sau đó trả về trạng thái no content
            departmentRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // Nếu có lỗi trả về null và trạng thái lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API để xóa tất cả phòng ban
    @DeleteMapping("/departments")
    public ResponseEntity<Department> deleteAllDepartment() {
        try {
            // dùng hàm deleteAll để xóa toàn bộ department sau đó trả về trạng thái no content
            departmentRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // Nếu có lỗi trả về null và trạng thái lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
