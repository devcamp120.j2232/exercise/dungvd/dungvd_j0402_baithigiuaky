package com.devcamp.midqualification.deparmentemployee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeparmentEmployeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeparmentEmployeeApplication.class, args);
	}

}
