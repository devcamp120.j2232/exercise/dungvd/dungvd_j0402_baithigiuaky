package com.devcamp.midqualification.deparmentemployee.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.midqualification.deparmentemployee.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{
    
}
